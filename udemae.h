//
// Created by 石川康貴 on 2015/10/27.
//

#ifndef UDEMAESIMULATION_UDEMAE_H
#define UDEMAESIMULATION_UDEMAE_H

#endif //UDEMAESIMULATION_UDEMAE_H


enum class Udemae{
    C_minus,
    C,
    C_plus,
    B_minus,
    B,
    B_plus,
    A_minus,
    A,
    A_plus,
    S1,
    S2,
    S3,
    S_plus1,
    S_plus2,
    S_plus3
};


enum class Fight{
    Win,
    Lose
};


int getChangedPoint(Udemae udemae, Fight fight){
        switch(udemae){
            case Udemae::C_minus :
                return (fight == Fight::Win) ? 20 : -10;

            case Udemae::C :
                return (fight == Fight::Win) ? 15 : -10;

            case Udemae::C_plus :
                return (fight == Fight::Win) ? 12 : -10;

            case Udemae::B_minus:
            case Udemae::B :
            case Udemae::B_plus :
            case Udemae::A_minus :
            case Udemae::A :
            case Udemae::A_plus :
                return (fight == Fight::Win) ? 10 : -10;

            case Udemae::S1 :
                return (fight == Fight::Win) ? 5 : -5;

            case Udemae::S2 :
                return (fight == Fight::Win) ? 4 : -5;

            case Udemae::S3 :
                return (fight == Fight::Win) ? 4 : -6;

            case Udemae::S_plus1 :
                return (fight == Fight::Win) ? 4 : -4;

            case Udemae::S_plus2 :
                return (fight == Fight::Win) ? 3 : -5;

            case Udemae::S_plus3 :
                return (fight == Fight::Win) ? 2 : -5;
        }
};