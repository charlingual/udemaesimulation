//
// Created by 石川康貴 on 2015/10/27.
//

#ifndef UDEMAESIMULATION_PLAYER_H
#define UDEMAESIMULATION_PLAYER_H


#include "udemae.h"
#define Real_Udemae_Upper 100 //リアルウデマエの上限

class Player {
private:
    unsigned int real_udemae;//実際には数値化されない上手さ、レンジは適当に
    const unsigned int joutatsu_rate;//何回やるとリアルウデマエが上がるか。
    unsigned int count;//上達してから何回やったか
    unsigned int udemae_rate;//0~99の数字
    bool count_stop_flag;//カンストした時にやめてしまうかどうか、やめてしまう場合true
    Udemae udemae;//S、S+はそれぞれ3段階に分けている

public:
    Player(unsigned int a, unsigned int b, bool c):real_udemae(a), joutatsu_rate(b), count_stop_flag(c){};
    unsigned int getRealUdemae();//リアルウデマエを返す
    void _judgeUdemae(Fight fight);//勝敗を受け取った時のウデマエ、レート変動
    void match(Fight fight);//勝敗を受け取った時の処理全て


};


#endif //UDEMAESIMULATION_PLAYER_H
