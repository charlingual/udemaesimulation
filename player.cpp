//
// Created by 石川康貴 on 2015/10/27.
//

#include "player.h"

void Player::_judgeUdemae(Fight fight) {
    int current_point = udemae_rate + getChangedPoint(udemae, fight);
    if(current_point < 0){
        if(udemae != Udemae::C_minus) {
            udemae = static_cast<Udemae >(static_cast<int>(udemae) - 1);
            udemae_rate = 70;
        }
        else{
            udemae_rate = 0;
        }
    }
    else if(99 < current_point){
        if(udemae != Udemae::S_plus3){
            udemae = static_cast<Udemae >(static_cast<int>(udemae) + 1);
            udemae_rate = 30;
        }
        else{
            udemae_rate = 99;
        }
    }
    else udemae_rate = (unsigned int)current_point;
}

void Player::match(Fight fight) {
    _judgeUdemae(fight);
    if(udemae == Udemae::S_plus3 && udemae_rate == 99 && count_stop_flag){
        //ここにカンストしたらやめてしまう場合を記述
    }
    if(count == joutatsu_rate){
        if(real_udemae != Real_Udemae_Upper) {
            ++real_udemae;
            count = 0;
        }
    }
    else ++count;
}

unsigned int Player::getRealUdemae() {
    return real_udemae;
}
